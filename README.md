Feature 1:

This repository contains the edited version of babycino which includes a greater or equal too feature. This feature has been added through different modifications. Specifically, the modifications done were, adding >= in the Antlr grammar, adding a class in the TACGenerator to allow for compiling to c, modifying TACOp.java to include cases for >=, and compromising the TACPeepholeOptimiser.java class to also include cases for >=.

The changes were tested using the GETester class which contains a simple program that includes the >= operator. The results were sucessful as shown by the GETester.c class which was produced after compiling. 

Feature 2:

3 test cases were added to the file progs to show that the function of do while loops are not performed properly. The first test case H1 shows that an integer can be used as a condition which should not be able to compile. The second test case shows that the condition of the while loop can be checked at the start, which should normally halt execution. The third test case shows that even though the condition is met the loop will still continue to iterate which should not normally happen.
