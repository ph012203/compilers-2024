class TestBugH1 {
    public static void main(String[] args) {
        int x = 1;
        do {
            System.out.println(x);
            x = x - 1;
        } while (x); // Bug H1: Using an integer as the loop condition
    }
}

