class TestBugH3 {
    public static void main(String[] args) {
        int x = 1;
        do {
            System.out.println("Loop executed");
            x = x - 1;
        } while (x == 0); // Bug H3: The loop should end when the condition is true, but if bugged, it might not.
    }
}

