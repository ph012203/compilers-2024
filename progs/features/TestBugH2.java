class TestBugH2 {
    public static void main(String[] args) {
        int x = 0;
        do {
            System.out.println("This should not print.");
        } while (x > 0); // Bug H2: The condition is checked at the start, so this should not execute.
    }
}

